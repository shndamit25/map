# Steps to run:
    In map.js replace csvUrl values "USA.csv" and "World.csv" with respective values.
    If CSV files are not on the same domain as your website, a CORS enabled server or a proxy is required.

# csv file format:
    value of south latitude should be negative(eg: 25.2744° S, 133.7751° E will be -25.2744,133.7751)
    value of west longitude should be negative(eg: 37.0902° N, 95.7129° W will be 37.0902,-95.7129)
    World.csv file can be referred for World Map
    USA.csv file can be referred for USA Map


# Demo url:
    https://shndamit25.github.io/Maps/

# testing setup:
    host the website as shown in testing.odt file    
    do not rename index.html file

# browser support
    For better experience following are the browser list with minimum versions
        Browsers        Version
        Chrome              70
        Opera               55
        Safari              11
        Firefox             50
        Edge                all
